@echo off
REM 수동 코드

tasklist | findstr -i nox
netstat -ano | findstr -i %위에서 찾은 PID%
adb connect 127.0.0.1:%위에서 찾은 PORT%

REM 쉘 연결
adb shell
REM 여러 장치일 때, 쉘 연결
adb -s 127.0.0.1:%위에서 찾은 PORT% shell

REM frida 버전 확인
frida --version
REM 아키텍처 확인
adb shell getprop ro.product.cpu.abi

REM 파일 push
mount -o remount,rw /system
adb push 9a5ba575.0 /system/etc/security/cacerts/
adb push frida-server-16.1.6-android-x86_64.xz /data/local/tmp/
mount -o remount,ro /system

REM 프리다 서버 설정
xz -d frida-server-16.1.6-android-x86_64.xz
chmod +x frida-server-16.1.6-android-x86_64
/data/local/tmp/frida-server-16.1.6-android-x86_64

REM Burp SSL 연결
chmod 644 /system/etc/security/cacerts/9a5ba575.0
REM Nox - 설정 - 보안 및 위치 - 암호화 및 사용자 인증 정보 - 신뢰할 수 있는 자격증명 - PortSwigger 체크 확인
REM Burp 설정 - Proxy listeners - Add - Bind to port를 80, Bind to address를 Specific address: 127.0.0.1로 설정
REM Burp 브라우저에서 chrome://inspect 이동 - Nox에서 인터넷 접속 - inspect로 원격 조종


@echo off
REM 자동 코드
REM 프리다가 설치되었는지 확인 필요 코드
REM adb가 설치되었는지 확인 필요 코드
REM adb device가 연결되었는지 확인 필요 코드
REM 에뮬레이터의 아키텍처의 확인 필요 코드
REM 프리다의 버전과 nox 에뮬레이터의 아키텍처에 맞는 frida-server가 설치되었는지 확인 필요 코드
REM nox_adb를 사용하지 않는 이유는 멀티 에뮬레이터 환경에서 동작하지 않음

tasklist | findstr -i nox
netstat -ano | findstr -i %위에서 찾은 PID%
adb connect 127.0.0.1:%위에서 찾은 PORT%
adb connect 127.0.0.1:62001
for /f "tokens=* delims='\''" %%i IN ('frida --version') do set frida_version=%%i
echo %frida_version%
if "%frida_version%"=="" (
    echo "frida가 설치되지 않았습니다."
    goto :EOF
)


for /f "tokens=*" %%i in ('nox_adb shell getprop ro.product.cpu.abi') do set architecture_version=%%i
for /f "tokens=*" %%i in ('nox_adb shell ls /data/local/tmp/frida-server-%frida_version%-android-%architecture_version%') do set file_exist=%%i
if "%file_exist%"=="" (
    echo https://github.com/frida/frida/releases로 이동하세요.
    echo frida-server-%frida_version%-android-%architecture_version%.xz 파일을 받고 안드로이드 /data/local/tmp/ 디렉터리에 옮기세요.
    echo xz -d frida-server-%frida_version%-android-%architecture_version%.xz 명령어로 압축을 해제하세요.
    echo chmod +x frida-server-%frida_version%-android-%architecture_version% 명령어로 실행 권한을 부여하세요.
    echo /data/local/tmp/frida-server-%frida_version%-android-%architecture_version% 명령어로 실행하세요.
) else (
    nox_adb shell /data/local/tmp/frida-server-%frida_version%-android-%architecture_version%
)



