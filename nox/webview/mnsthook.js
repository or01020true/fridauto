// > frida -U -l mnsthook.js -F com.sesac.mnstbank

setImmediate(function() {
    Java.perform(function() {
        let CheckRoot = Java.use("com.sesac.mnstbank.CheckRoot");
        CheckRoot["isRooted"].implementation = function () {
            return false;
        };

        var search_string = ""; // 찾을 문자열
        let MainActivity = Java.use("com.sesac.mnstbank.MainActivity");
        MainActivity["onBackPressed"].implementation = function () {
            // 뒤로 가기 버튼
            // var search_string = ['JSESSIONID=', 'JWT='];
            var search_string = ['299985000'];
            
            search_string.forEach(function (patt, index) {
                var pattern = patt.split('').map(char => char.charCodeAt(0).toString(16)).join(' ');
                Process.enumerateRanges('rw-', {
                    onMatch: function (range) {

                        var result = Memory.scanSync(range.base, range.size, pattern);
                        if (result.length > 0) {
                        
                            result.forEach(function (match) {
                                console.log("");
                                console.log("\x1b[31m" + '[*] Scan String: ' + patt + "\x1b[0m");
                                console.log("\x1b[36m" + '[*] String Address: ' + match.address + "\x1b[0m");									                     
                                console.log(hexdump(match.address, { offset: 0, length: 128 }));
                            });
                        }
                    },
                    onComplete: function () {}
                });
            });

            
        };
    });
});