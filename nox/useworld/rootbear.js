setImmediate(function() {
    Java.perform(function() {
        let RootBeer = Java.use("com.scottyab.rootbeer.RootBeer");
        RootBeer["checkForBinary"].implementation = function (str) {
            console.log(`RootBeer.checkForBinary is called: str=${str}`);
            let result = this["checkForBinary"](str);
            console.log(`RootBeer.checkForBinary result=${result}`);
            return false;
        };
    
        RootBeer["checkSuExists"].implementation = function (str) {
            console.log(`RootBeer.checkSuExists is called: str=${str}`);
            let result = this["checkSuExists"](str);
            console.log(`RootBeer.checkSuExists result=${result}`);
            return false;
        };
    });
});