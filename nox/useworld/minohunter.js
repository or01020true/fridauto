// > frida -U -l minohunter.js -f ksy.useworlds.minohunter
setImmediate(function() {
    Java.perform(function() {
        // let GameView = Java.use("ksy.useworlds.minohunter.GameView");
        // GameView["checkKill"].implementation = function () {
            
        // };

        // Java.choose("ksy.useworlds.minohunter.GameView", {
        //     onMatch : function(instance) {
        //         instance.checkKill();
        //     }, onComplete: function() {}
        // });

        Interceptor.attach(Java.use("ksy.useworlds.minohunter.GameView")["checkKill"].implementation, {
            onEnter: function (args) {
                console.log('checkKill() 함수에 진입했습니다.');        
                this.aimX = this.mino.getX();
                console.log('인자를 수정했습니다.');
            }
        });
    });
});